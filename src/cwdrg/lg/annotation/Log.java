package cwdrg.lg.annotation;

import groovy.lang.GroovyClassLoader;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.FieldNode;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.BooleanExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.TernaryExpression;
import org.codehaus.groovy.transform.GroovyASTTransformationClass;
import org.codehaus.groovy.transform.LogASTTransformation;
import org.objectweb.asm.Opcodes;

/**
 * Similarly to @Slf4j....
 * This local transform adds the Lg wrapper around the Slf4j facade and
 * LogBack logging. Every method call on a unbound variable named <i>log</i>
 * will be mapped to a call to the logger. For this a <i>log</i> field will be
 * inserted in the class. If the field already exists the usage of this transform
 * will cause a compilation error. The method name will be used to determine
 * what to call on the logger.
 * <p>
 * <pre>
 * log.name(exp)
 * </pre>
 * <p>
 * is mapped to
 * <p>
 * <pre>
 * if (log.isNameLoggable() {
 *    log.name(exp)
 * }
 * </pre>
 * <p>
 * Here name is a place holder for info, debug, warning, error, etc.
 * If the expression exp is a constant or only a variable access the method call will
 * not be transformed. But this will still cause a call on the injected logger.
 *
 * @author Plagiarized um adapted by CEM
 * @since 1.8.0
 */
@java.lang.annotation.Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
@GroovyASTTransformationClass("org.codehaus.groovy.transform.LogASTTransformation")
public @interface Log {
    String value() default "lg";

    Class<? extends LogASTTransformation.LoggingStrategy> loggingStrategy() default LgSlf4jLoggingStrategy.class;

    class LgSlf4jLoggingStrategy extends LogASTTransformation.AbstractLoggingStrategy {
        private static final String LG_LOGGER_NAME = "cwdrg.lg.Lg";

        private static final String SLF_FACTORY_NAME = "org.slf4j.LoggerFactory";

        protected LgSlf4jLoggingStrategy(final GroovyClassLoader loader) {
            super(loader);
        }

        public FieldNode addLoggerFieldToClass(ClassNode classNode, String logFieldName) {
            // private transient final static Lg lg = new Lg(LoggerFactory.getLogger(classname))
            return classNode.addField(
                    logFieldName,
                    Opcodes.ACC_FINAL | Opcodes.ACC_TRANSIENT | Opcodes.ACC_STATIC | Opcodes.ACC_PRIVATE,
                    classNode(LG_LOGGER_NAME),
                    new ConstructorCallExpression(
                            classNode(LG_LOGGER_NAME),
                            new MethodCallExpression(
                                    new ClassExpression(
                                            classNode(SLF_FACTORY_NAME)
                                    ),
                                    "getLogger",
                                    new ClassExpression(classNode)
                            )
                    )
            );
        }

        public boolean isLoggingMethod(String methodName) {
            return methodName.matches("err|wrn|inf|dbg|trc");
        }

        public Expression wrapLoggingMethodCall(Expression logVariable, String methodName, Expression originalExpression) {
            System.out.println("wrapping  " + methodName);
            MethodCallExpression condition = new MethodCallExpression(logVariable, methodName.substring(0, 1), ArgumentListExpression.EMPTY_ARGUMENTS);

            return new TernaryExpression(new BooleanExpression(condition), originalExpression, ConstantExpression.NULL);
        }

        // TODO: figure out three param vs two param version... why?
        public FieldNode addLoggerFieldToClass(ClassNode classNode, String logFieldName, String unknown) {
            return addLoggerFieldToClass(classNode, logFieldName);
        }

    }

}